package queueHW;



public class Queue extends Thread {
	private int totalWait;
	private int actualWait;
	private int totalNumberOfClients;
	private int actualNumberOfClients;
	Client served;
	private int servedTime;
	Client last;
	private int state;
	public static String textField="";
	private Timer t;
	private int id;
	private int emptyQueueTime=0;
	
	Queue(Timer t,int id){
		state=1;
		served=null;
		last=null;
		totalWait=0;
		actualWait=0;
		totalNumberOfClients=0;
		actualNumberOfClients=0;
		this.t=t;
		this.id=id;
	}
	
	public String toString(){
		if (served==null)
			return " ";
		String text="<html>";
		Client temp=served;
		while(temp!=null)
		{
			text+="serv: "+temp.getServTime()+" wait: "+temp.getWaitTime()+"<br>";
			temp=temp.getNext();
		}
		text+="</html>";
		return text;
	}
	void addClient(Client added)
	{
		if(served==null)
		{
			served=added;
			last=added;
			servedTime=added.getServTime();
			served.setServed(true);
		}
		else{
			totalWait+=last.getServTime();
			last.setNext(added);
			last=added;
		}
		actualWait+=added.getServTime();
		totalNumberOfClients++;
		actualNumberOfClients++;
		
		textField+=("queue "+id+" : the client was added at the time "+added.getArrTime()+"\n");
	}
	void pop(int time){
		if(served!=null)
		{
			actualWait-=servedTime;
			actualNumberOfClients--;
			textField+=("queue "+id+" : the client left at the time "+t.getTime()+" and waited for "+served.getWaitTime()+"\n");
			served.leave(time);
			served=served.getNext();
			if(served!=null){
				servedTime=served.getServTime();
				served.setServed(true);
			}
			else
				servedTime=0;
		}
	}
	
	public void stopit(){
		state=0;
	}
	public void restart(){
		state=2;
	}
	public void run(){
		while(state!=0)
		{
			while (state==1)
				try {
					sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (state==2){
				if(served!=null)
				{
					Client temp=served;
					while(temp!=null){
						temp.secPass();
						temp=temp.getNext();
					}
					if(served.getServTime()==0)
						pop(t.getTime());
				}
				else
					this.emptyQueueTime++;
				state=1;
			}
			
		}
	}
	
	int getActualWait(){
		return this.actualWait;
	}
	int getAverageWaitTime(){
		return this.totalWait/(this.totalNumberOfClients+1);
	}
	int getNrOfClients(){
		return this.actualNumberOfClients;
	}
	int getEmptyQueueTime(){
		return this.emptyQueueTime;
	}
}
