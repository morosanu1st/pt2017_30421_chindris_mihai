package queueHW;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class SimulationWindow extends Thread{

	private JFrame logger;
	private JFrame frame;
	private JTextArea loggerText;
	private Manager manager;
	private JLabel[] queues;
	private JLabel[] clientsAtQueue;
	private JLabel time;
	private JLabel actualTime;
	private int nrOfQueues;
	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the application.
	 */
	public SimulationWindow(Manager manager) {
		this.manager=manager;
		this.nrOfQueues=manager.getNrOfQueues();
		queues=new JLabel[nrOfQueues];
		this.clientsAtQueue=new JLabel[nrOfQueues];
		initialize();
	}
	public void run(){
		
		long t=System.currentTimeMillis();
		while(manager.isAlive())
		{
			if(System.currentTimeMillis()>t+1000){
				t=System.currentTimeMillis();
			for(int i=0;i<nrOfQueues;i++){
				clientsAtQueue[i].setBounds(i*100+100, 150, 89, 23*manager.getQueues()[i].getNrOfClients());
				clientsAtQueue[i].setText(""+manager.getQueues()[i].toString());
				
				}
			loggerText.setText(Queue.textField);
			actualTime.setText(""+manager.getTime());}
		}
		loggerText.setText(Queue.textField);
	}
	public void makeVisible(){
		frame.setVisible(true);
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Simulation");
		frame.setBounds(50 , 50 , nrOfQueues*100+200, 350);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		logger=new JFrame("Log");
		logger.setBounds(1000 , 50 , 360, 650);
		logger.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		logger.getContentPane().setLayout(null);
		
		loggerText=new JTextArea();
		loggerText.setEditable(false);
		loggerText.setBounds(0,0, 350, 700);
		loggerText.setAutoscrolls(true);
		
		
		logger.getContentPane().add(loggerText);
		logger.setVisible(true);
		
		time=new JLabel("Time: ");
		time.setBounds(nrOfQueues*50, 40, 89, 23);
		frame.getContentPane().add(time);
		actualTime=new JLabel();
		actualTime.setBounds(nrOfQueues*50+100, 40, 89, 23);
		frame.getContentPane().add(actualTime);
		System.out.println("we're good");
		for (int i=0;i<nrOfQueues;i++)
		{
			queues[i] = new JLabel("Queue "+(i+1));
			queues[i].setBounds(i*100+100, 100, 89,23);
			frame.getContentPane().add(queues[i]);
			this.clientsAtQueue[i]=new JLabel();
			clientsAtQueue[i].setBounds(i*100+100, 150, 89, 200);
			frame.getContentPane().add(clientsAtQueue[i]);
		}
		JLabel l=new JLabel("");
		l.setBounds(nrOfQueues*100, 150, 89, 23);
		frame.add(l);
	}

}
