package queueHW;

public class Manager extends Thread {
	Timer t;
	public String textField="";
	private Queue[] queues;
	private int peakHour;
	private int maxClients=0;
	private int totalService=0;
	private int nrOfQueues;
	private int minService;
	 int deltaService;
	private int minGap;
	 int maxGap;
	int time=0;
	private int nrOfClients=0;
	
	Manager(int simulationTime,int nrOfQueues,int minGap,int maxGap,int minService,int maxService){
		t=new Timer(simulationTime);
		queues=new Queue[nrOfQueues];
		for(int i=nrOfQueues-1;i>=0;i--)
			queues[i]=new Queue(t,i);
		this.nrOfQueues=nrOfQueues;
		this.minService=minService;
		deltaService=maxService-minService;
		this.minGap=minGap;
		this.maxGap=maxGap;
		
	}
	public void addClient(int time){
		Client added=new Client(time,minService+(int)(Math.random()*(deltaService)));
		int min=queues[0].getActualWait();
		int j=0;
		for(int i=nrOfQueues-1;i>0;i--)
			if(queues[i].getActualWait()<min){
				min=queues[i].getActualWait();
				j=i;
			}
		queues[j].addClient(added);
		this.nrOfClients++;
		totalService+=queues[j].last.getServTime();
	}
	
	void doit(int tillNextClient,int next)
	{
		if (t.getTime()>time){
			if(tillNextClient==0)
			{
				addClient(time);
				tillNextClient=minGap+(int)(Math.random() * (next+1 ))-1;
			}
			else
				tillNextClient--;
			
			int currentNrOfClients=queues[0].getNrOfClients();
			for(int i=nrOfQueues-1;i>0;i--)
					currentNrOfClients+=queues[i].getNrOfClients();
			if (currentNrOfClients>this.maxClients){
				maxClients=currentNrOfClients;
				peakHour=time;
			}
			for(int i=nrOfQueues-1;i>=0;i--)
				queues[i].restart();
			time=t.getTime();
		}
	}
	public void run(){
		for(int i=nrOfQueues-1;i>=0;i--)
			queues[i].start();
		
		int next=maxGap-minGap;
		int tillNextClient=minGap+(int)(Math.random() * (next+1))-1;
		t.start();
		while (t.isAlive())
			doit(tillNextClient,next);
		
		int averageWait=0;
		for(int i=this.nrOfQueues-1;i>=0;i--)
			averageWait+=queues[i].getAverageWaitTime();
		averageWait/=nrOfQueues;
		int averageEmpty=0;
		for(int i=this.nrOfQueues-1;i>=0;i--)
			averageEmpty+=queues[i].getEmptyQueueTime();
		averageEmpty/=nrOfQueues;
		Queue.textField+=("The simulation just finished\nStats:\ntotal clients: "+nrOfClients+" \naverage waiting time was: "+averageWait+"\naverage empty queue time was: "+averageEmpty+"\nAverage service time was: "+this.getAverageService()+"\npeak hour was: "+peakHour+ " when there were "+maxClients+" clients");
		for(int i=this.nrOfQueues-1;i>=0;i--)
			queues[i].stopit();
		System.out.println(Queue.textField);
		
	}
	public Timer getT() {
		return t;
	}
	
	public Queue[] getQueues() {
		return queues;
	}
	
	public int getPeakHour() {
		return peakHour;
	}
	
	public int getNrOfQueues(){
		return this.nrOfQueues;
	}
	
	public int getAverageService(){
		if(this.nrOfClients!=0)
			return totalService/this.nrOfClients;
		else return 0;
	}
	
	public int getTime(){
		return time;
	}
	
	
}
