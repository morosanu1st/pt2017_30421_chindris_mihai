import javax.swing.JTable;

public interface BankProc {
	public void restoreObservers();
	
	/*
	 * Adds a new client to the bank
	 * @pre there is no existing client with that name
	 * @post the bank size+=1
	 * @post there will be a client with that name*/
	public void addClient(String name,int phoneNmuber);
	
	/*
	 * deletes a client from the bank
	 * @pre there exists a client named name
	 * @post all the clients accounts will be deleted
	 * @post the client will no longer be in the bank
	 * @post the bank size -=1
	 */
	public void removeClient(String name);
	
	/*
	 * creates a new account of type type associated with the client holderand returns its id
	 * @pre there exists a client with the name holder
	 * @pre balance>=0
	 * @pre type="saving"||type="spending"
	 * @pre period>0
	 * @pre interest>0
	 * @post a new account is added to the holders list of accounts
	 * @post the numbers of accounts is incremented
	 */
	public int createAccount(String holder,int balance,String type,int period,Integer interest );
	
	/*
	 * deletes the account that has accountId=id 
	 * @pre the account exists
	 * @post the account is removed
	 */
	public void deleteAccount(String holder,Account account);
	
	/*
	 * returns a JTable of all the clients of the bank
	 * @pre none
	 * @post none
	 */
	public JTable listAllClients();
	
	/*
	 * returns the table of all the accounts associated with the holder holder
	 * @pre client exists
	 * @post none
	 */
	public JTable listAssociatedAccounts(String holder);
	
	
	/*
	 * @pre account.balance+ammount>=0;
	 * @post account.balance+=ammount
	 */
	public int modifyAccountBy(String holder,Account account,int ammount);
	
	/*
	 * @pre the holder exists
	 * @pre the account is a sving Account||the account hasnt been changed
	 * @post the new holder's name is newName
	 */
	public  void changeHolderName(String holder,String newHolder);
}
