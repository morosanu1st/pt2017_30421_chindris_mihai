import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class MainWindow {
	Bank bank=null;
	JLabel lblMessage = new JLabel("");
	private JFrame frame;
	private ListClientsWindow clientsTableFrame;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 262, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblMessage = new JLabel("");
		lblMessage.setBounds(10, 232, 226, 14);
		frame.getContentPane().add(lblMessage);
		
		 try {
	         FileInputStream fileIn = new FileInputStream("bank.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         bank = (Bank) in.readObject();
	         in.close();
	         fileIn.close();
	      }catch(IOException i) {
	        lblMessage.setText("A new bank was created");
	         bank=new Bank();
	      }catch(ClassNotFoundException c) {
	         System.out.println("Employee class not found");
	         c.printStackTrace();
	         return;
	      }
		 bank.restoreObservers();
		
		
		JButton btnAddClient = new JButton("Add Client");
		btnAddClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddClientWindow frame=new AddClientWindow();
				frame.setVisible(true);
			}
		});
		btnAddClient.setBounds(47, 42, 160, 23);
		frame.getContentPane().add(btnAddClient);
		
		JButton btnRemoveClient = new JButton("Remove client");
		btnRemoveClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RemoveClientWindow frame=new RemoveClientWindow();
				frame.setVisible(true);
			}
		});
		btnRemoveClient.setBounds(47, 76, 160, 23);
		frame.getContentPane().add(btnRemoveClient);
		
		JButton btnListAllClients = new JButton("List all clients\r\n");
		btnListAllClients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clientsTableFrame =new ListClientsWindow();
				clientsTableFrame.setVisible(true);
			}
		});
		btnListAllClients.setBounds(47, 110, 160, 23);
		frame.getContentPane().add(btnListAllClients);
		
		JButton btnChangeHolderName = new JButton("Change holder name");
		btnChangeHolderName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ChangeHolderNameWindow frame=new ChangeHolderNameWindow();
				frame.setVisible(true);
			}
		});
		btnChangeHolderName.setBounds(47, 144, 160, 23);
		frame.getContentPane().add(btnChangeHolderName);
		
		JButton btnSaveChanges = new JButton("Save changes");
		btnSaveChanges.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
			         FileOutputStream fileOut =
			         new FileOutputStream("bank.ser");
			         ObjectOutputStream out = new ObjectOutputStream(fileOut);
			         out.writeObject(bank);
			         out.close();
			         fileOut.close();
			         lblMessage.setText("The bank was saved in bank.ser\n");
			      }catch(IOException i) {
			         i.printStackTrace();
			      }
			}
		});
		btnSaveChanges.setBounds(63, 198, 127, 23);
		frame.getContentPane().add(btnSaveChanges);
		
		
	}
	
	class AddClientWindow extends JFrame {

		private JPanel contentPane;
		private JTextField name;
		private JTextField phone;
		JLabel lblMessage = new JLabel("\r\n");


		public AddClientWindow() {
			setTitle("Add New Client");
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 172, 263);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			name = new JTextField();
			name.setBounds(10, 27, 115, 20);
			contentPane.add(name);
			name.setColumns(10);
			
			phone = new JTextField();
			phone.setBounds(10, 70, 115, 20);
			contentPane.add(phone);
			phone.setColumns(10);
			
			JLabel lblName = new JLabel("Name");
			lblName.setBounds(10, 2, 46, 14);
			contentPane.add(lblName);
			
			JLabel lblPhoneNumber = new JLabel("Phone number");
			lblPhoneNumber.setBounds(10, 45, 96, 14);
			contentPane.add(lblPhoneNumber);
			
			JButton btnAdd = new JButton("Add");
			btnAdd.setBounds(24, 125, 89, 23);
			btnAdd.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					lblMessage.setText("");
					int i=0;
					try {
						i=Integer.parseInt(phone.getText());
					} catch (NumberFormatException e1) {
						lblMessage.setText("invalid number");
					}
					if(i!=0)
					{
						bank.addClient(name.getText(), i);
						lblMessage.setText("Client added");
					}
				}});
			contentPane.add(btnAdd);
			
			
			lblMessage.setBounds(24, 175, 122, 14);
			contentPane.add(lblMessage);
		}
	}
	
	private class RemoveClientWindow extends JFrame {

		private JPanel contentPane;
		private JTextField textField;
		JLabel lblMessage = new JLabel("");

		public RemoveClientWindow() {
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 161, 200);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			JLabel lblName = new JLabel("Name");
			lblName.setBounds(39, 29, 46, 14);
			contentPane.add(lblName);
			
			textField = new JTextField();
			textField.setBounds(25, 54, 86, 20);
			contentPane.add(textField);
			textField.setColumns(10);
			
			JButton btnDeleteClient = new JButton("Delete");
			btnDeleteClient.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					lblMessage.setText("");
					bank.removeClient(textField.getText());
					lblMessage.setText("client removed");
				}
			});
			btnDeleteClient.setBounds(25, 107, 89, 23);
			contentPane.add(btnDeleteClient);
			
			
			lblMessage.setBounds(0, 141, 135, 14);
			contentPane.add(lblMessage);
		}
	}
	
	private class ChangeHolderNameWindow extends JFrame {

		private JPanel contentPane;
		private JTextField holder;
		private JTextField textField;
		private JLabel lblMessage = new JLabel("\r\n");
		
		public ChangeHolderNameWindow() {
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 222, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			holder = new JTextField();
			holder.setBounds(34, 31, 86, 20);
			contentPane.add(holder);
			holder.setColumns(10);
			
			JLabel lblHolder = new JLabel("Holder");
			lblHolder.setBounds(34, 6, 46, 14);
			contentPane.add(lblHolder);
			
			textField = new JTextField();
			textField.setBounds(34, 82, 86, 20);
			contentPane.add(textField);
			textField.setColumns(10);
			
			JLabel lblNewName = new JLabel("New Name");
			lblNewName.setBounds(34, 57, 214, 14);
			contentPane.add(lblNewName);
			
			JButton btnUpdate = new JButton("Update");
			btnUpdate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					lblMessage.setText("");
					bank.changeHolderName(holder.getText(), textField.getText());
					lblMessage.setText("Name changed");
				}
			});
			btnUpdate.setBounds(31, 145, 89, 23);
			contentPane.add(btnUpdate);
			
			
			lblMessage.setBounds(34, 202, 86, 14);
			contentPane.add(lblMessage);
		}

	}
	
	private class ListClientsWindow extends JFrame {

		private JPanel contentPane;
		private JTable table;
		private  ClientWindow clientFrame;

		
		public ListClientsWindow() {
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 450, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(0, 0, 434, 262);
			contentPane.add(scrollPane);
			
			table = bank.listAllClients();
			scrollPane.setViewportView(table);
			table.setRowSelectionAllowed(true);
			table.setDefaultEditor(Object.class, null);
			table.setColumnSelectionAllowed(false);
			table.addMouseListener(new MouseAdapter() {
			    public void mousePressed(MouseEvent me) {
			        JTable tableT =(JTable) me.getSource();
			        Point p = me.getPoint();
			        int row = tableT.rowAtPoint(p);
			        if (me.getClickCount() == 2) {
			            int i=tableT.getSelectedRow();
			            String s=tableT.getValueAt(i, 0).toString();
			            clientFrame=new ClientWindow(s);
			            clientFrame.setVisible(true);
			        }
			    }});
			table.setVisible(true);
		}
		
		private class ClientWindow extends JFrame {
			AddAccountWindow addFrame;
			private JPanel contentPane;
			private String holder;
			ListAssociatedAccountsWindow accTableFrame;
			
			public ClientWindow(String holder) {
				this.holder=holder;
				Client client=bank.clients.get(holder);
				
				setTitle(holder);
				setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				setBounds(100, 100, 273, 169);
				contentPane = new JPanel();
				contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
				setContentPane(contentPane);
				contentPane.setLayout(null);
				
				JButton btnAddAccount = new JButton("Add Account");
				btnAddAccount.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						addFrame=new AddAccountWindow();
						addFrame.setVisible(true);
					}
				});
				btnAddAccount.setBounds(84, 11, 122, 23);
				contentPane.add(btnAddAccount);
				
				
				JButton btnListAccounts = new JButton("List accounts");
				btnListAccounts.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						accTableFrame=new ListAssociatedAccountsWindow(holder);
						accTableFrame.setVisible(true);
					}
				});
				btnListAccounts.setBounds(84, 79, 122, 23);
				contentPane.add(btnListAccounts);
			}
			
			private class AddAccountWindow extends JFrame {

				private JPanel contentPane;
				private JTextField type;
				private JTextField ballance;
				private JTextField period;
				private JTextField interest;
				private JLabel lblMessage;
				private JButton btnAdd;
				private JLabel lblType;
				private JLabel lblInitialDeposit;
				private JLabel lblPeriodmonths;
				private JLabel lblinterest;

				
				public AddAccountWindow() {
					setTitle("Create account");
					setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					setBounds(100, 100, 450, 169);
					contentPane = new JPanel();
					contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
					setContentPane(contentPane);
					contentPane.setLayout(null);
					
					type = new JTextField();
					type.setBounds(10, 29, 86, 20);
					contentPane.add(type);
					type.setColumns(10);
					
					ballance = new JTextField();
					ballance.setBounds(106, 29, 86, 20);
					contentPane.add(ballance);
					ballance.setColumns(10);
					
					period = new JTextField();
					period.setBounds(202, 29, 86, 20);
					contentPane.add(period);
					period.setColumns(10);
					
					interest = new JTextField();
					interest.setBounds(298, 29, 86, 20);
					contentPane.add(interest);
					interest.setColumns(10);
					
					lblMessage = new JLabel("");
					lblMessage.setBounds(26, 92, 358, 14);
					contentPane.add(lblMessage);
					
					btnAdd = new JButton("Add");
					btnAdd.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							int p,b,i=0;
							if(!(type.getText().equals("Saving")||type.getText().equals("Spending")))
							{
								lblMessage.setText("Invalid type (valid types: Saving or Spending)");
								return;
							}
							try {
								 b=Integer.parseInt(ballance.getText());
							} catch (NumberFormatException e1) {
								lblMessage.setText("Invalid initial deposit");
								return;
							}
							try {
								 p=Integer.parseInt(period.getText());
							} catch (NumberFormatException e1) {
								lblMessage.setText("Invalid period");
								return;
							}
							if(type.getText().equals("Saving"))
							try {
								i=Integer.parseInt(interest.getText());
							} catch (NumberFormatException e1) {
								lblMessage.setText("Invalid period");
								return;
							}
							else
								lblMessage.setText("*The interest is only applicable to saving accounts");
							bank.createAccount(holder, b, type.getText(), p, i);
							addFrame.dispose();
						}
					});
					btnAdd.setBounds(157, 58, 89, 23);
					contentPane.add(btnAdd);
					
					lblType = new JLabel("Type");
					lblType.setBounds(10, 11, 46, 14);
					contentPane.add(lblType);
					
					lblInitialDeposit = new JLabel("Initial deposit");
					lblInitialDeposit.setBounds(106, 11, 86, 14);
					contentPane.add(lblInitialDeposit);
					
					lblPeriodmonths = new JLabel("Period(months)");
					lblPeriodmonths.setBounds(202, 11, 100, 14);
					contentPane.add(lblPeriodmonths);
					
					lblinterest = new JLabel("*Interest");
					lblinterest.setBounds(299, 11, 100, 14);
					contentPane.add(lblinterest);
				}

			}
			
			class ListAssociatedAccountsWindow extends JFrame {

				private JPanel contentPane;
				private JTable table;
				AccountWindow acWindow;

				
				public ListAssociatedAccountsWindow(String holder) {
					setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					setBounds(100, 100, 650, 300);
					contentPane = new JPanel();
					contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
					setContentPane(contentPane);
					contentPane.setLayout(null);
					
					JScrollPane scrollPane = new JScrollPane();
					scrollPane.setBounds(0, 0, 634, 262);
					contentPane.add(scrollPane);
					
					table = bank.listAssociatedAccounts(holder);
					scrollPane.setViewportView(table);
					table.setRowSelectionAllowed(true);
					table.setDefaultEditor(Object.class, null);
					table.setColumnSelectionAllowed(false);
					table.addMouseListener(new MouseAdapter() {
					    public void mousePressed(MouseEvent me) {
					        JTable tableT =(JTable) me.getSource();
					        Point p = me.getPoint();
					        int row = tableT.rowAtPoint(p);
					        if (me.getClickCount() == 2) {
					            int i=tableT.getSelectedRow();
					            String s=tableT.getValueAt(i, 0).toString();
					            int id=Integer.parseInt(s);
					           ArrayList<Account> acList=bank.accounts.get(bank.clients.get(holder));
					           Account ac=acList.get(0);
					           int j=0;
					           while(ac.getId()!=id)
					        	   ac=acList.get(++j);
					           acWindow=new AccountWindow(ac,holder);
					           acWindow.setVisible(true);
					            
					        }
					    }});
					table.setVisible(true);
				}
				
				private class AccountWindow extends JFrame {

					private JPanel contentPane;
					private JTextField deposit;
					private JButton btnDeposit;
					private JButton btnWithdraw;
					private JLabel lblSum;
					private JLabel lblWarn = new JLabel("");

					public AccountWindow(Account account,String holder) {
						setTitle("Account id:"+account.getId());
						setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						setBounds(100, 100, 268, 198);
						contentPane = new JPanel();
						contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
						setContentPane(contentPane);
						contentPane.setLayout(null);
						
						JButton btnDeleteAccount = new JButton("Delete Account");
						btnDeleteAccount.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								bank.deleteAccount(holder, account);
								acWindow.dispose();
								accTableFrame.dispose();
								clientsTableFrame.dispose();
								clientFrame.dispose();
							}
						});
						btnDeleteAccount.setBounds(61, 11, 156, 23);
						contentPane.add(btnDeleteAccount);
						
						deposit = new JTextField();
						deposit.setBounds(82, 67, 86, 20);
						contentPane.add(deposit);
						deposit.setColumns(10);
						
						btnDeposit = new JButton("Deposit");
						btnDeposit.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								lblWarn.setText("");
								if(account.getAccountType().equals("Saving")){
									lblWarn.setText("illegal on saving accounts");
									return;
								}
								
								int ammount;
								try {
									ammount = Integer.parseInt(deposit.getText());
								} catch (NumberFormatException e1) {
									lblWarn.setText("Invalid sum");
									return;
								}
								bank.modifyAccountBy(holder, account, ammount);
								acWindow.dispose();
								accTableFrame.dispose();
								clientsTableFrame.dispose();
								clientFrame.dispose();
							}
						});
						btnDeposit.setBounds(10, 98, 89, 23);
						contentPane.add(btnDeposit);
						
						btnWithdraw = new JButton("Withdraw");
						btnWithdraw.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if(account.getAccountType().equals("Saving")){
									int i=bank.modifyAccountBy(holder, account, 0);
									lblWarn.setText("Account closed. "+i+" were withdrawn");
									acWindow.dispose();
									accTableFrame.dispose();
									clientsTableFrame.dispose();
									clientFrame.dispose();
									return;
								}
								int ammount;
								try {
									ammount = Integer.parseInt(deposit.getText());
								} catch (NumberFormatException e1) {
									lblWarn.setText("Invalid sum");
									return;
								}
								bank.modifyAccountBy(holder, account, -ammount);
								acWindow.dispose();
								accTableFrame.dispose();
								clientsTableFrame.dispose();
								clientFrame.dispose();
							}
						});
						btnWithdraw.setBounds(153, 98, 89, 23);
						contentPane.add(btnWithdraw);
						
						lblSum = new JLabel("Sum");
						lblSum.setBounds(82, 42, 46, 14);
						contentPane.add(lblSum);
						
						lblWarn.setBounds(42, 120, 250, 14);
						contentPane.add(lblWarn);
					}

				}

			}

		}
	}

	
	
}
