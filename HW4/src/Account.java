import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Observable;

public abstract class Account extends Observable implements Serializable{
	private static int lastId=1;
	protected int accountId;
	protected int ballance;
	private Client holder;
	protected LocalDateTime creationTime;
	private int period;
	
	public Account(Client holder,int balance,int period)
	{
		accountId=lastId;
		lastId++;
		this.period=period;
		this.holder=holder;
		this.addObserver(holder);
		this.ballance=balance;
		creationTime=LocalDateTime.now();
	}
	
	public int getId(){
		return accountId;
	}
	
	public abstract  String getAccountType();
	
	public int getBallance(){
		this.access("get ballance");
		return ballance;
	}
	
	public LocalDateTime getCreationTime(){
		this.access("get creation time");
		return this.creationTime;
	}
	
	public int getPeriod(){
		this.access("get period");
		return period;
	}
	public void setPeriod(int period){
		this.period=period;
		this.access("set period");
	}
	
	public boolean isActive(){
	LocalDateTime temp=creationTime.plusMonths(period);
	this.access("Get state");
	if(temp.compareTo(LocalDateTime.now())>0)
		return true;
	return false;
		
	}
	
	public void access(String type){
		this.setChanged();
		Message message=new Message(accountId,"Access",type);
		this.notifyObservers(message);
		this.clearChanged();
	}
	
	
	
}
