import java.io.Serializable;

public class Message implements Serializable {
	private int id;
	private String type;
	private Object info;
	
	
	public Message(int id,String type, Object info) {
		this.id=id;
		this.type = type;
		this.info = info;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Object getInfo() {
		return info;
	}
	public void setInfo(Object info) {
		this.info = info;
	}
	
}
