
public class SpendingAccount extends Account {


	public SpendingAccount(Client holder, int balance, int period) {
		super(holder, balance, period);
	}

	@Override
	public String getAccountType() {
		return "Spending";
	}
	
	public void setBallance(int ball)
	{
		this.setChanged();
		Message message=new Message(accountId,"ChangeBy",(int)(ball-ballance));
		this.notifyObservers(message);
		this.clearChanged();
		super.ballance=ball;
	}
	
}
