package assignment1;

public class Polynomial {
	private int degree;
	private double[] coeff;
	
	Polynomial (int degree,double[] coeff)
	{
		this.degree=degree;
		this.coeff=new double[degree+1];
		for(int i=0;i<=degree;i++)
			this.coeff[i]=coeff[i];
	}
	Polynomial(int degree)
	{
		this.coeff=new double[degree+1];
		this.degree=degree;
		for(int i=0;i<=degree;i++)
			coeff[i]=0;
	}
	
	
	public String toString()
	{
		String temp="Your polynomial is: ";
		for (int i=0;i<=degree;i++){
			temp=temp+(int)coeff[i]+"x^"+i;
			if (degree>i)
				if(coeff[i+1]>=0)
					temp=temp+" + ";
		}
		return temp;
	}
	
	
	
	public Polynomial add(Polynomial p2)
	{
		Polynomial result;
		int maxdeg=Math.max(p2.degree,degree);
		int mindeg=Math.min(p2.degree,degree);
		if(this.degree==maxdeg)
		{
			 result=new Polynomial(maxdeg,this.coeff);
			 for (int i=0;i<=mindeg;i++)
				 result.coeff[i]+=p2.coeff[i];
		}
		else{
			result=new Polynomial(maxdeg,p2.coeff);
			for (int i=0;i<=mindeg;i++)
				 result.coeff[i]+=this.coeff[i];
		}
		return result;
	}
	public Polynomial subtract(Polynomial p2)
	{
		Polynomial result;
		int maxdeg=Math.max(p2.degree,degree);
		int mindeg=Math.min(p2.degree,degree);
		if(this.degree==maxdeg)
		{
			 result=new Polynomial(maxdeg,this.coeff);
			 for (int i=0;i<=mindeg;i++)
				 result.coeff[i]-=p2.coeff[i];
		}
		else{
			result=new Polynomial(maxdeg,p2.coeff);
			for (int i=0;i<=mindeg;i++)
				 result.coeff[i]-=this.coeff[i];
			for (int i=0;i<=maxdeg;i++)
				result.coeff[i]*=-1;
		}
		result.fix();
		return result;
	}
	
	void fix()
	{
		
		while(coeff[degree]==0)
			degree--;
	}
	
	public Polynomial multiply(Polynomial p2)
	{
		Polynomial result=new Polynomial(p2.degree+this.degree);
		for (int i=0;i<=this.degree;i++)
			for(int j=0;j<=p2.degree;j++)
				result.coeff[i+j]+=this.coeff[i]*p2.coeff[j];
		
		return result;
	}
	
	
	public static void main(String[] args)
	{
		
		
	}
}
