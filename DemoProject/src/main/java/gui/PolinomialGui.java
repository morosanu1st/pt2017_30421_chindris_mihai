package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.awt.event.ActionEvent;

public class PolinomialGui extends JFrame {

	private JPanel contentPane;
	private JTextField textField1;
	private JTextField textField2;
	private JTextField textField3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PolinomialGui frame = new PolinomialGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PolinomialGui() {		
		setTitle("Calculator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 850, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPolynomial = new JLabel("Polynomial1");
		lblPolynomial.setBounds(27, 57, 116, 14);
		contentPane.add(lblPolynomial);
		
		JLabel lblPolynomial_1 = new JLabel("Polynomial2");
		lblPolynomial_1.setBounds(486, 57, 109, 14);
		contentPane.add(lblPolynomial_1);
		
		textField1 = new JTextField();
		textField1.setToolTipText("type some integers separated by spaces");
		textField1.setBounds(27, 82, 260, 20);
		contentPane.add(textField1);
		textField1.setColumns(10);
		
		textField2 = new JTextField();
		textField2.setToolTipText("Insert some integers separated by spaces");
		textField2.setBounds(486, 82, 307, 20);
		contentPane.add(textField2);
		textField2.setColumns(10);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.setBounds(257, 133, 56, 23);
		contentPane.add(btnAdd);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int j=0;
				String[] in1=textField1.getText().trim().split(" ");
				String[] in2=textField2.getText().trim().split(" ");
				
				try{
				double[] arr1=new double[in1.length];
				for(int i=0;i<arr1.length;i++){
					arr1[i]=Double.parseDouble(in1[i]);
					if (arr1[i]!=Math.floor(arr1[i]))
							j=1;
					}
				
				double[] arr2=new double[in2.length];
				for(int i=0;i<arr2.length;i++){
					arr2[i]=Double.parseDouble(in2[i]);
					if (arr2[i]!=Math.floor(arr2[i]))
						j=1;
					}
				if(j==0){	
				Polynomial p1=new Polynomial(arr1.length-1,(double[])arr1);
				Polynomial p2=new Polynomial(arr2.length-1,(double[])arr2);
				textField3.setText(p1.add(p2).toString());
				}
				else
					textField3.setText("please make sure that all the coefficients are integers");
				}
				catch(java.lang.NumberFormatException ex){
					textField3.setText("Please make sure that you typed in the polynomials corectly (i.e. integers separated by commas)");
				}
		}});
		
		JButton btnNewButton = new JButton("Subtract");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int j=0;
				String[] in1=textField1.getText().trim().split(" ");
				String[] in2=textField2.getText().trim().split(" ");
				
				try{
				double[] arr1=new double[in1.length];
				for(int i=0;i<arr1.length;i++){
					arr1[i]=Double.parseDouble(in1[i]);
					if (arr1[i]!=Math.floor(arr1[i]))
							j=1;
					}
				
				double[] arr2=new double[in2.length];
				for(int i=0;i<arr2.length;i++){
					arr2[i]=Double.parseDouble(in2[i]);
					if (arr2[i]!=Math.floor(arr2[i]))
						j=1;
					}
				if(j==0){	
				Polynomial p1=new Polynomial(arr1.length-1,(double[])arr1);
				Polynomial p2=new Polynomial(arr2.length-1,(double[])arr2);
				textField3.setText(p1.subtract(p2).toString());
				}
				else
					textField3.setText("please make sure that all the coefficients are integers");
				}
					catch(java.lang.NumberFormatException ex){
						textField3.setText("Please make sure that you typed in the polynomials corectly (i.e. integers separated by commas)");
					}
		}});
		btnNewButton.setBounds(374, 133, 99, 23);
		contentPane.add(btnNewButton);
		
		JButton btnMultiply = new JButton("Multiply");
		btnMultiply.setBounds(514, 133, 89, 23);
		contentPane.add(btnMultiply);
		btnMultiply.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int j=0;
				String[] in1=textField1.getText().trim().split(" ");
				String[] in2=textField2.getText().trim().split(" ");
				
				try{
				double[] arr1=new double[in1.length];
				for(int i=0;i<arr1.length;i++){
					arr1[i]=Double.parseDouble(in1[i]);
					if (arr1[i]!=Math.floor(arr1[i]))
							j=1;
					}
				
				double[] arr2=new double[in2.length];
				for(int i=0;i<arr2.length;i++){
					arr2[i]=Double.parseDouble(in2[i]);
					if (arr2[i]!=Math.floor(arr2[i]))
						j=1;
					}
				if(j==0){	
				Polynomial p1=new Polynomial(arr1.length-1,(double[])arr1);
				Polynomial p2=new Polynomial(arr2.length-1,(double[])arr2);
				textField3.setText(p1.multiply(p2).toString());
				}
				else
					textField3.setText("please make sure that all the coefficients are integers");
				}
					catch(java.lang.NumberFormatException ex){
						textField3.setText("Please make sure that you typed in the polynomials corectly (i.e. integers separated by commas)");
					}
		}});
		
		JButton btnDivide = new JButton("Divide");
		btnDivide.setBounds(670, 133, 89, 23);
		contentPane.add(btnDivide);
		btnDivide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int j=0;
				String[] in1=textField1.getText().trim().split(" ");
				String[] in2=textField2.getText().trim().split(" ");
				
				try{
				double[] arr1=new double[in1.length];
				for(int i=0;i<arr1.length;i++){
					arr1[i]=Double.parseDouble(in1[i]);
					if (arr1[i]!=Math.floor(arr1[i]))
							j=1;
					}
				
				double[] arr2=new double[in2.length];
				for(int i=0;i<arr2.length;i++){
					arr2[i]=Double.parseDouble(in2[i]);
					if (arr2[i]!=Math.floor(arr2[i]))
						j=1;
					}
				if(j==0){	
				Polynomial p1=new Polynomial(arr1.length-1,(double[])arr1);
				Polynomial p2=new Polynomial(arr2.length-1,(double[])arr2);
				textField3.setText("quotient: "+p1.divide(p2)[0].toString()+"    rest: "+p1.divide(p2)[1].toString());
				}
				else
					textField3.setText("please make sure that all the coefficients are integers");
				}
					catch(java.lang.NumberFormatException ex){
						textField3.setText("Please make sure that you typed in the polynomials corectly (i.e. integers separated by commas)");
					}		
	}});
		JButton btnIntegrate = new JButton("Integrate");
		btnIntegrate.setBounds(10, 133, 89, 23);
		contentPane.add(btnIntegrate);
		btnIntegrate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] in1=textField1.getText().trim().split(" ");
				int j=0;
				try{
				double[] arr1=new double[in1.length];
				for(int i=0;i<arr1.length;i++){
					arr1[i]=Double.parseDouble(in1[i]);
					if (arr1[i]!=Math.floor(arr1[i]))
							j=1;
					}
				if(j==0){	
					Polynomial p1=new Polynomial(arr1.length-1,(double[])arr1);
					textField3.setText(p1.integrate().toString());
					}
					else
						textField3.setText("please make sure that all the coefficients are integers");
					}
				catch(java.lang.NumberFormatException ex)
				{
					textField3.setText("Please make sure that you typed in the polynomials corectly (i.e. integers separated by commas)");
				}		
		}});
		
		JButton btnDifferentiate = new JButton("Differentiate");
		btnDifferentiate.setBounds(123, 133, 106, 23);
		contentPane.add(btnDifferentiate);
		btnDifferentiate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] in1=textField1.getText().trim().split(" ");
				
				
				int j=0;
				try{
				double[] arr1=new double[in1.length];
				for(int i=0;i<arr1.length;i++){
					arr1[i]=Double.parseDouble(in1[i]);
					if (arr1[i]!=Math.floor(arr1[i]))
							j=1;
					}
				if(j==0){	
					Polynomial p1=new Polynomial(arr1.length-1,(double[])arr1);
					textField3.setText(p1.differentiate().toString());
					}
					else
						textField3.setText("please make sure that all the coefficients are integers");
					}
					catch(java.lang.NumberFormatException ex)
					{
						textField3.setText("Please make sure that you typed in the polynomials corectly (i.e. integers separated by commas)");
					}
		}});
		
		textField3 = new JTextField();
		textField3.setBounds(27, 231, 766, 20);
		contentPane.add(textField3);
		textField3.setColumns(10);
		
		JLabel lblResult = new JLabel("Result");
		lblResult.setBounds(353, 186, 46, 14);
		contentPane.add(lblResult);
		
		JLabel help = new JLabel("Type the polynomial's coefficients separated by spaces in ascending order");
		help.setBounds(231, 11, 500, 14);
		contentPane.add(help);
		
	}
}
