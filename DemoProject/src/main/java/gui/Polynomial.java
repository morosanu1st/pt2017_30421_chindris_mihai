package gui;

import java.io.IOException;
import java.text.DecimalFormat;
public class Polynomial {
	private int degree;
	private double[] coeff;
	
	Polynomial (int degree,double[] coeff)
	{
		this.degree=degree;
		this.coeff=new double[degree+1];
		for(int i=0;i<=degree;i++)
			this.coeff[i]=coeff[i];
	}
	Polynomial(int degree)
	{
		this.coeff=new double[degree+1];
		this.degree=0;
	}
	Polynomial()
	{
		this(0);
	}	
	
	private double getLead()
	{
		return coeff[degree];
	}
	
	public String toString()
	{
		String temp="";
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		for (int i=0;i<=degree;i++){
			if(coeff[i]!=0&&coeff[i]!=1){
			if(i==0)
				temp=temp+df.format(coeff[i]);
			else if(i==1)
				temp=temp+df.format(coeff[i])+"x";
			else
				temp=temp+df.format(coeff[i])+"x^"+i;
			if (degree>i)
				if(coeff[i+1]>=0)
					temp=temp+" + ";
				else
					temp+=" ";
			}
			if(coeff[i]==1){
				if(i==0)
					temp=temp+df.format(coeff[i]);
				else
				 if(i==1)
					temp=temp+"x";
				else
					temp=temp+"x^"+i;
				if (degree>i)
					if(coeff[i+1]>=0)
						temp=temp+" + ";
					else
						temp+=" ";
				}
				
		}
		if(this.isZero())
			temp+=" 0";
		return temp;
	}
	private boolean isZero()
	{
		for(int i=0;i<=degree;i++)
			if(coeff[i]!=0)
				return false;
		return true;
	}
	
	
	public Polynomial add(Polynomial p2)
	{
		Polynomial result;
		int maxdeg=Math.max(p2.degree,degree);
		int mindeg=Math.min(p2.degree,degree);
		if(this.degree==maxdeg)
		{
			 result=new Polynomial(maxdeg,this.coeff);
			 for (int i=0;i<=mindeg;i++)
				 result.coeff[i]+=p2.coeff[i];
		}
		else{
			result=new Polynomial(maxdeg,p2.coeff);
			for (int i=0;i<=mindeg;i++)
				 result.coeff[i]+=this.coeff[i];
		}
		return result;
	}
	public Polynomial subtract(Polynomial p2)
	{
		Polynomial result;
		int maxdeg=Math.max(p2.degree,degree);
		int mindeg=Math.min(p2.degree,degree);
		if(this.degree==maxdeg)
		{
			 result=new Polynomial(maxdeg,this.coeff);
			 for (int i=0;i<=mindeg;i++)
				 result.coeff[i]-=p2.coeff[i];
		}
		else{
			result=new Polynomial(maxdeg,p2.coeff);
			for (int i=0;i<=mindeg;i++)
				 result.coeff[i]-=this.coeff[i];
			for (int i=0;i<=maxdeg;i++)
				result.coeff[i]*=-1;
		}
		result.fixDown();
		return result;
	}
	
	private void fixDown()
	{
		degree=coeff.length-1;
		while(coeff[degree]==0.0&&degree>0)
			degree--;
	}
	private void fixUp()
	{
		for(int i=0;i<this.coeff.length;i++)
			if(this.coeff[i]!=0)
				this.degree=i;
	}
	
	public Polynomial multiply(Polynomial p2)
	{
		Polynomial result=new Polynomial(p2.degree+this.degree);
		for (int i=0;i<=this.degree;i++)
			for(int j=0;j<=p2.degree;j++){
				result.coeff[i+j]= result.coeff[i+j]+this.coeff[i]*p2.coeff[j];
				
			}
		result.fixUp();
		return result;
	}
	Polynomial multiplyM(int pow, double coeff)
	{
		Polynomial result=new Polynomial(this.degree+pow);
		for(int i=0;i<=this.degree;i++)
			result.coeff[i+pow]=this.coeff[i]*coeff;
		result.fixUp();
		return result;
	}
	
	public Polynomial[] divide(Polynomial p2)
	{
		Polynomial[] result=new Polynomial[2];
		
		if (this.degree<p2.degree)
		{
			result[0]=new Polynomial();
			result [1]=new Polynomial(this.degree,this.coeff);
			return result;
		}
		Polynomial q=new Polynomial(this.degree);
		Polynomial r=new Polynomial(this.degree,this.coeff);
		int pow;
		double co;
		while(!r.isZero()&&r.degree>=p2.degree)
		{
			pow=r.degree-p2.degree;
			co= r.getLead()/p2.getLead();
			q.coeff[pow]=co;
			q.fixUp();
			r=r.subtract(p2.multiplyM(pow,co));
			r.fixDown();
			r.fixUp();
			
		}
		
		result[0]=q;
		result[1]=r;
		return result;
	}
	Polynomial integrate()
	{
		Polynomial result=new Polynomial(degree+1);
		for(int i=0;i<=degree;i++)
		{
			result.coeff[i+1]=coeff[i]/(i+1);
		}
		result.fixUp();
		return result;
	}
	Polynomial differentiate()
	{
		
		if(degree>0)
		{
		Polynomial result=new Polynomial(degree-1);
		for(int i=1;i<=degree;i++)
			result.coeff[i-1]=coeff[i]*(i);
		result.fixUp();
		return result;
		}	
		return new Polynomial();
		
	}
	/*public static void main(String[] args)
	{
		double[] test={7,8,9,10};
		double[] t1={5,3,2};
		Polynomial s=new Polynomial(3,test);
		Polynomial p=new Polynomial(2,t1);
		Polynomial result[]=s.divide(p);
		Polynomial q=s.differentiate();
		
		System.out.println(q);
		
		
		
	}*/
}
