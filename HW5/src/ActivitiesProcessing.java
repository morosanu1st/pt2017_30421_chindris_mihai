import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ActivitiesProcessing {
	public static long getDistinctDays(ArrayList<MonitoredData> data){
		Set<Integer> s=new HashSet<Integer>();
		data.stream().forEach(x->{s.add(x.startTime.getDayOfYear());s.add(x.endTime.getDayOfYear());});
		return s.size();
	}
	
	public static Object getActivityCount(ArrayList<MonitoredData> data,String retType){
		Map<String,Integer> map=new HashMap<String,Integer>();
		ArrayList<String> labels=new ArrayList<String>();
		data.stream().forEach(x->{
			if(!map.containsKey(x.activityLabel)){
				map.put(x.activityLabel, 1);
				labels.add(x.activityLabel);
			}
			else
				map.put(x.activityLabel,map.get(x.activityLabel)+1);
		});
		if(retType.equals("Pair"))
			return new Pair<ArrayList<String>,Map<String,Integer>>(labels,map);
		else
		{
			String out=new String("");
			for(String x:labels)
				out+="Activity: "+x+"\r\nCount: "+map.get(x)+"\r\n\r\n\r\n";
			return out;
		}
			
	}
	
	public static Object getActivityCountPerDay(ArrayList<MonitoredData> data,String retType){
		Map<Integer,Pair<ArrayList<String>,Map<String,Integer>>> map=new HashMap<Integer,Pair<ArrayList<String>,Map<String,Integer>>>();
		List<LocalDate> dates=data.stream().map(x->x.startTime.toLocalDate()).distinct().collect(Collectors.toList());
		Map<Integer,ArrayList<MonitoredData>> separated=new HashMap<Integer,ArrayList<MonitoredData>>();
		data.stream().forEach(x->{
			if(!separated.containsKey(x.startTime.getDayOfYear())){
				ArrayList<MonitoredData> neu=new ArrayList<MonitoredData>();
				neu.add(x);
				separated.put(x.startTime.getDayOfYear(), neu);
			}
			else
				separated.get(x.startTime.getDayOfYear()).add(x);
		});
		dates.stream().forEach(x->{
			map.put(x.getDayOfYear(), ((Pair<ArrayList<String>,Map<String,Integer>>)getActivityCount(separated.get(x.getDayOfYear()),"Pair")));
		});
		if(retType.equals("Pair"))
			return map;
		else
		{
			String ret=new String("");
			for(LocalDate x:dates)
				ret+=x.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))+"\r\n"+(String)getActivityCount(separated.get(x.getDayOfYear()),"String")+"\r\n\r\n\r\n";
			return ret;
		}
		
	}
	public static Object getTotalTime(ArrayList<MonitoredData> data,String type){
		Map<String,LocalDateTime> timed=new HashMap<String,LocalDateTime>();
		ArrayList<String> labels=new ArrayList<String>();
		data.stream().forEach(x->{
			if(!timed.containsKey(x.activityLabel)){
				timed.put(x.activityLabel, x.endTime.minusYears(x.startTime.getYear()).minusDays(x.startTime.getDayOfYear()).minusHours(x.startTime.getHour()).minusMinutes(x.startTime.getMinute()).minusSeconds(x.startTime.getSecond()));
				labels.add(x.activityLabel);
			}
			else
				timed.put(x.activityLabel, timed.get(x.activityLabel)
						.plusDays(x.endTime.getDayOfYear()).minusDays(x.startTime.getDayOfYear()).plusSeconds(x.endTime.getSecond()).minusSeconds(x.startTime.getSecond()).plusMinutes(x.endTime.getMinute()).minusMinutes(x.startTime.getMinute()).plusHours(x.endTime.getHour()).minusHours(x.startTime.getHour()));
		});
		List<String> filtered=labels.stream().filter(x->{
			if(timed.get(x).getDayOfYear()>1){timed.remove(x);
				return false;}
			if(timed.get(x).getHour()>=10){timed.remove(x);
				return false;}
			return true;
		}).collect(Collectors.toList());
		if(type.equals("Pair"))
			return new Pair<List<String>,Map<String,LocalDateTime>>(filtered,timed);
		String out=new String("");
		for(String x:filtered)
			out+="Activity: "+x+" Duration:\r\nDays: "+(timed.get(x).getDayOfYear()-1)+" \tHours: "+timed.get(x).getHour()+"\t Minutes: "+timed.get(x).getMinute()+" \tSeconds: "+timed.get(x).getSecond()+"\r\n\r\n\r\n";
		return out;
	}
	public static Object getLongerActivities(ArrayList<MonitoredData> data,String type){
		Map<String,Integer> filteredCount=new HashMap<String,Integer>();
		Map<String,Integer> totalCount=((Pair<ArrayList<String>,Map<String,Integer>>)getActivityCount(data,"Pair")).r;;
		List<String> out=new ArrayList<String>();
		data.stream().filter(x->{
		LocalDateTime t=x.endTime.minusYears(x.startTime.getYear()).minusDays(x.startTime.getDayOfYear()).minusHours(x.startTime.getHour()).minusMinutes(x.startTime.getMinute()).minusSeconds(x.startTime.getSecond());
		if(t.compareTo(LocalDateTime.parse("0000-01-01T00:05:00"))>0)return true;return false;
		}).forEach(x->{
		if(filteredCount.containsKey(x.activityLabel))
			filteredCount.put(x.activityLabel,totalCount.get(x.activityLabel)+1);
		else
				filteredCount.put(x.activityLabel, 1);});
		data.stream().map(x->x.activityLabel).distinct().filter(x->{
			Double f=0.0;
			if(filteredCount.containsKey(x))
				f=filteredCount.get(x).doubleValue();
			else
				System.out.println("it looks like there is no "+x+"lasting more than 5 minutes");
			Double t=totalCount.get(x).doubleValue();
			if(f/t<=0.9) return false; return true;
		}).forEach(x->out.add(x));;
		if(type.equals("Pair"))
			return out;
		return "Filtered Activities: "+out.toString();
	}
	public static void write2file(String type,String data){
		
		try {
			PrintWriter writer;
			writer = new PrintWriter(type+".txt", "UTF-8");
			writer.println(data);
		    writer.close();
		} catch ( IOException e) {
			e.printStackTrace();
		} 
	}
	public static Map<String, Long> variant2(ArrayList<MonitoredData> data)
	{
		return data.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.counting()));
	}
	public static void main(String[] args)
	{
		ArrayList<MonitoredData> activities=file2data();
		System.out.println("there is a total of "+getDistinctDays(activities)+" monitored days");
		write2file("Activity count",(String)getActivityCount(activities,"String"));
		write2file("Activity count on days",(String)getActivityCountPerDay(activities, "String"));
		write2file("Activity duration",(String)getTotalTime(activities, "String"));
		write2file("Filtered activities",(String)getLongerActivities(activities,"String"));
		//Map<String,Long> m=variant2(activities);
		//m.keySet().stream().forEach(x->System.out.println("activity: "+x+ " count: "+m.get(x)));
		System.out.println("c'est fini");
	}
	public static ArrayList<MonitoredData> file2data(){
		try {
			return  Files.lines(Paths.get("d:/Faculta/PT/PT2017_30421_mihai_chindris/HW5", "Act.txt")).map(x->{	String[] s=x.split("\\s{2,}");return new MonitoredData(s[0],s[1],s[2]);}).collect(Collectors.toCollection(ArrayList::new));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
