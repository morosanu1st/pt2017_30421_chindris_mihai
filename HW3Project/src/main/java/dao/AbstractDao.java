package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connector.ConnectionFactory;

public class AbstractDao<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDao.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDao() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	 String createSelectQuery(String field,String value) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM wharehouse.");
		sb.append(type.getSimpleName());
		if(field!="")
			sb.append(" WHERE " + field + "= '"+value+"'");
		return sb.toString();
	}
	 
	 public ArrayList<T> selectQuery(String fields,String value){
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			String Query = createSelectQuery(fields,value);
			try {
				connection = ConnectionFactory.getConnection();
				statement = connection.prepareStatement(Query);
				resultSet = statement.executeQuery();
				return createObjects(resultSet);
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, type.getName() + "DAO:SELECT " + e.getMessage());
			} finally {
				ConnectionFactory.close(resultSet);
				ConnectionFactory.close(statement);
				ConnectionFactory.close(connection);
			}
			return null;
		} 
	 String createSelectAllQuery() {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM ");
			sb.append(type.getSimpleName());
			return sb.toString();
		}
	 public ArrayList<T> selectAllQuery(){
		 Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			String Query = createSelectAllQuery();
			try {
				connection = ConnectionFactory.getConnection();
				statement = connection.prepareStatement(Query);
				resultSet = statement.executeQuery();
				return createObjects(resultSet);
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, type.getName() + "DAO:SELECT " + e.getMessage());
			} finally {
				ConnectionFactory.close(resultSet);
				ConnectionFactory.close(statement);
				ConnectionFactory.close(connection);
			}
			return null;
	 }
	 String createDeleteQuery(String db,String field,String value){
		 StringBuilder sb = new StringBuilder();
			sb.append("DELETE FROM ");
			/*sb.append(db);
			sb.append(".");*/
			sb.append(type.getSimpleName());
			sb.append(" WHERE " + field + " = '"+value+"'");
			return sb.toString();
	 }
	 public int deleteQuery(String field,String value){
			Connection connection = null;
			PreparedStatement statement = null;
			String query = createDeleteQuery("wharehouse",field,value);
			try {
				connection = ConnectionFactory.getConnection();
				statement = connection.prepareStatement(query);
				 statement.executeUpdate();
				return 1;
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, type.getName() + "DAO:DELETE " + e.getMessage());
			} finally {
				ConnectionFactory.close(statement);
				ConnectionFactory.close(connection);
			}
			return 0;
		}
	 
	 String createInsertQuery(T obj,String db){
		 StringBuilder sb = new StringBuilder();
		 
			sb.append("INSERT INTO wharehouse.");
			/*sb.append(db);
			sb.append(".");*/
			sb.append(type.getSimpleName());
			sb.append(" ");
			sb.append(obj.toString());
			return sb.toString();
	 }
	 public T insertQuery(T obj,String db){
			Connection connection = null;
			PreparedStatement statement = null;
			String query = createInsertQuery(obj,db);
			try {
				connection = ConnectionFactory.getConnection();
				statement = connection.prepareStatement(query);
				statement.executeUpdate();
				return obj;
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, type.getName() + "DAO:INSERT " + e.getMessage());
			} finally {
				ConnectionFactory.close(statement);
				ConnectionFactory.close(connection);
			}
			return null;
		}
	 
	 String createUpdateQuery(List<Field> changeField,List<String> changeValue,String field,String value,String db){
		 StringBuilder sb = new StringBuilder();
		 
			sb.append("UPDATE ");
			/*sb.append(db);
			sb.append(".");*/
			sb.append(type.getSimpleName());
			sb.append(" ");
			sb.append(" SET " );
			int size=changeField.size();
			for(int i=0;i<size-1;i++){
				if(changeValue.get(i).length()!=0&&changeValue.get(i)!="-1"){
					
				sb.append(changeField.get(i).getName());
				sb.append(" = '");
				sb.append(changeValue.get(i));
				sb.append("', ");}
			}
			if (changeValue.get(size - 1).length()!=0&&changeValue.get(size - 1)!="-1") {
				sb.append(changeField.get(size - 1).getName());
				sb.append(" = '");
				sb.append(changeValue.get(size - 1));
			}
				
			sb.append("' WHERE ");
			sb.append(field);
			sb.append(" = '");
			sb.append(value);
			sb.append("'");
		
			return sb.toString();
	 }
	 public int updateQuerry(List<Field> changeField,List<String> changeValue,String field,String value,String db){
			Connection connection = null;
			PreparedStatement statement = null;
			String query = createUpdateQuery(changeField,changeValue,field,value,db);
			try {
				connection = ConnectionFactory.getConnection();
				statement = connection.prepareStatement(query);
				statement.executeUpdate();
				return 1;
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, type.getName() + "DAO:Update " + e.getMessage());
			} finally {
				ConnectionFactory.close(statement);
				ConnectionFactory.close(connection);
			}
			return 0;
		}
	 
	 
	public List<T> findAll() {
		// TODO:
		return null;
	}
	
	public Object[][] createData(){
		ArrayList<T> list=this.selectQuery("", "");
		Object[][] data=new Object[list.size()][type.getDeclaredFields().length];
		int i=0;
		for(T instance:list){
			try {
				
				
				int j=0;
				for (Field field : type.getDeclaredFields()) {
					PropertyDescriptor prop=new PropertyDescriptor(field.getName(),type);
					Method met=prop.getReadMethod();
					Object o=met.invoke(instance);
					
					data[i][j]=o;
					j++;					
				}
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IntrospectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i++;
			
		}
		
		return data;
	}
	
	public Object[][] createSomeData(){
		ArrayList<T> list=this.selectQuery("stock<", "5");
		Object[][] data=new Object[list.size()][type.getDeclaredFields().length];
		int i=0;
		for(T instance:list){
			try {
				
				
				int j=0;
				for (Field field : type.getDeclaredFields()) {
					PropertyDescriptor prop=new PropertyDescriptor(field.getName(),type);
					Method met=prop.getReadMethod();
					Object o=met.invoke(instance);
					
					data[i][j]=o;
					j++;					
				}
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IntrospectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i++;
			
		}
		
		return data;
	}

	 ArrayList<T> createObjects(ResultSet resultSet) {
		ArrayList<T> list = new ArrayList<T>();
		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	public T insert(T t) {
		// TODO:
		return t;
	}

	public T update(T t) {
		// TODO:
		return t;
	}
}
