package model;

public class ProductCount {
	private Integer orderID;
	private String product;
	private Integer count;
	public Integer getOrderID() {
		return orderID;
	}
	public void setOrderID(Integer orderID) {
		this.orderID = orderID;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "(orderID , product , count) VALUES ('" + orderID + "','" + product + "','" + count + "')";
	}
	
}
